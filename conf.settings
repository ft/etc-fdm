### This is used to set the maximum size of a mail. Mails larger than
### this limit are dropped and, if applicable, not deleted from the
### server.
set maximum-size 128M

### If this option is specified, fdm(1) attempts to delete messages
### which exceed maximum-size, and continue.If it is not specified,
### oversize messages are a fatal error and cause fdm(1) to abort.
#set delete-oversized

### If this option is specified, fdm(1) does not attempt to create a
### lock file and allows multiple instances to run simultaneously.
#set allow-multiple

### This sets an alternative lock file. The default is ~/.fdm.lock
### for non-root users and /var/db/fdm.lock for root.
set lock-file "${cfgdir}/fdm.lock"

### This specifies the locks to be used for mbox locking.  Possible
### types are fcntl, flock, and dotlock. The flock and fcntl types
### are mutually exclusive. The default is flock.
#set lock-types "flock"

### This sets the default user to change to before delivering mail,
### if fdmis running as root and no alternative user is specified as
### part of the action or rule.
#set default-user "hawk"

### This specifies the domains to be used when looking for users with
### the from-headers keyword. The default is the computer's hostname.
#set domain "bewatermyfriend.org"

### This allows the headers to be examined when looking for users to
### be set. The default is to look only at the "From" and "Cc"
### headers. The headers are case-insensitive.
#set header

### This instructs fdm to proxy all connections through url. HTTP
### and SOCKS5 proxies are supported at present (URLs of the form
### http://host[:port] or socks://[user:pass@]host[:port]).
### No authentication is supported for HTTP.
#set proxy

### This option controls what fdm does with mail that reaches the
### end of the ruleset (mail that matches no rules or matches only
### rules with the continue keyword). drop will cause such mail to
### be discarded, and keep will attempt to leave the mail on the
### server.  The default is to keep the mail and log a warning that
### it reached the end of the ruleset.
set unmatched-mail keep

### This option makes fdm attempt to purge deleted mail from the
### server (if supported) after count mails have been retrieved.
set purge-after 10

### If set, fdm will not insert a 'Received' header into each mail.
#set no-received

### This specifies the umask(2) to use when creating files. 'user'
### means to use the umask set when fdm is started, or umask may be
### specified as a three-digit octal number.
set file-umask 077

### This option allows the default group ownership of files and
### directories created by fdm(1) to be specified. 'group' may be a
### group name string or a numeric gid. 'user' does nothing.
set file-group user

### This controls the maximum time to wait for a server to send data
### before closing a connection. The default is 900 seconds.
set timeout 900

### Instructs fdm to verify SSL certificates for all SSL connections.
set verify-certificates

set queue-high 12
set queue-high 5
